package com.jimo.btree2;

/**
 * @author jimo
 * @date 2018/9/18 13:40
 * @code 每个b树的节点，含有很多entry
 */
public final class Node {
    /**
     * 一个节点里key或者说entry的数量
     */
    int numOfKey;
    /**
     * 代表孩子节点
     */
    Entry[] children;

    /**
     * @author jimo
     * @date 2018/9/18 13:48
     * @code m代表树的阶
     */
    public Node(int numOfKey, int m) {
        this.numOfKey = numOfKey;
        children = new Entry[m];
    }
}
