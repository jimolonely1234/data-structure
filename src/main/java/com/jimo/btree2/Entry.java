package com.jimo.btree2;

public class Entry {
    Comparable key;
    final Object value;
    Node next;

    public Entry(Comparable key, Object value, Node next) {
        this.key = key;
        this.value = value;
        this.next = next;
    }
}
