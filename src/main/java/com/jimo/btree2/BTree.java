package com.jimo.btree2;

/**
 * @author jimo
 * @date 2018/9/18 10:09
 * @code B树
 */
public class BTree<Key extends Comparable<Key>, Value> {
	/**
	 * 代表最大孩子数，所以每个节点最大key数为m-1
	 */
	private int m;
	private Node root;
	/**
	 * 树的高度，同时具有判断是否是叶子节点的作用
	 */
	private int height;

	public BTree(int m) {
		this.m = m;
		root = new Node(0, m);
	}

	public Value search(Key key) {
		if (key == null) {
			throw new IllegalArgumentException("key不能为null");
		}
		return search(root, key, height);
	}

	private Value search(Node x, Key key, int h) {
		final Entry[] children = x.children;
		if (h == 0) {
			/*叶子节点*/
			for (int i = 0; i < x.numOfKey; i++) {
				if (eq(key, children[i].key)) {
					return (Value) children[i].value;
				}
			}
		} else {
			/*内部节点*/
			for (int i = 0; i < x.numOfKey; i++) {
				/*如果到最后一个entry或者找到适合key插入的位置，就继续向子节点搜索*/
				if (i + 1 == x.numOfKey || less(key, children[i + 1].key)) {
					return search(children[i].next, key, h - 1);
				}
			}
		}
		return null;
	}

	private boolean eq(Comparable k1, Comparable k2) {
		return k1.compareTo(k2) == 0;
	}

	private boolean less(Comparable k1, Comparable k2) {
		return k1.compareTo(k2) < 0;
	}

	public void insert(Key key, Value value) {
		if (key == null) {
			throw new IllegalArgumentException("key不能为null");
		}
		final Node u = insert(root, key, value, height);
		if (u == null) {
			return;
		}

		/*拆分后需要new一个新的父节点p*/
		final Node p = new Node(2, m);
		p.children[0] = new Entry(root.children[0].key, null, root);
		p.children[1] = new Entry(u.children[0].key, null, u);
		root = p;
		height++;
	}

	private Node insert(Node n, Key key, Value value, int h) {
		int i;
		final Entry t = new Entry(key, value, null);
		if (h == 0) {
			/*叶子节点*/
			for (i = 0; i < n.numOfKey; i++) {
				if (less(key, n.children[i].key)) {
					break;
				}
			}
		} else {
			/*内部节点*/
			for (i = 0; i < n.numOfKey; i++) {
				if (i + 1 == n.numOfKey || less(key, n.children[i + 1].key)) {
					final Node u = insert(n.children[i++].next, key, value, h - 1);
					if (u == null) {
						return null;
					}
					t.key = u.children[0].key;
					t.next = u;
					break;
				}
			}
		}
		for (int j = n.numOfKey; j > i; j--) {
			n.children[j] = n.children[j - 1];
		}
		n.children[i] = t;
		n.numOfKey++;
		if (n.numOfKey < m) {
			return null;
		} else {
			return split(n);
		}
	}

	private Node split(Node n) {

		// m的奇偶性分开处理
		if (m % 2 == 0) {
			final Node t = new Node(m / 2, m);
			n.numOfKey = m / 2;
			for (int i = 0; i < m / 2; i++) {
				t.children[i] = n.children[m / 2 + i];
			}
			return t;
		} else {
			final Node t = new Node(m / 2 + 1, m);
			for (int i = 0; i < m / 2; i++) {
				t.children[i] = n.children[m / 2 + i];
			}
			t.children[m / 2] = n.children[n.numOfKey - 1];
			n.numOfKey = m / 2;
			return t;
		}
	}

	@Override
	public String toString() {
		return toString(root, height, "") + "\n";
	}

	private String toString(Node h, int ht, String indent) {
		StringBuilder s = new StringBuilder();
		Entry[] children = h.children;

		if (ht == 0) {
			for (int j = 0; j < h.numOfKey; j++) {
				s.append(indent).append(children[j].key).append(" ")
						.append(children[j].value).append("\n");
			}
		} else {
			for (int j = 0; j < h.numOfKey; j++) {
				s.append(indent).append("(").append(children[j].key).append(")\n");
				s.append(toString(children[j].next, ht - 1, indent + "     "));
			}
		}
		return s.toString();
	}
}
