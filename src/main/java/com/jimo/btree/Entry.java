package com.jimo.btree;

/**
 * @author jimo
 * @date 2018/10/8 11:33
 */
public class Entry {
	Comparable key;
	Object value;

	public Entry(Comparable key, Object value) {
		this.key = key;
		this.value = value;
	}
}
