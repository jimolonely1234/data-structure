package com.jimo.btree;

/**
 * @author jimo
 * @date 2018/9/17 8:56
 * @code B树的实现
 */
public class BTree<Key extends Comparable, Value> {
	/**
	 * 根节点
	 */
	private Node root;
	/**
	 * 度
	 */
	private int k;

	public BTree(int k) {
		this.k = k;
		this.root = null;
	}

	public void traverse() {
		if (root != null) {
			traverse(root);
		}
	}

	/**
	 * 遍历一个节点及其子树, 中序遍历，所以是有序的
	 *
	 * @author jimo
	 * @date 2018/9/17 9:11
	 */
	public void traverse(Node n) {
		int i;
		for (i = 0; i < n.numOfKey; i++) {
			if (!n.leaf) {
				traverse(n.points[i]);
			}
			System.out.println(" " + n.entries[i].key);
		}
		/*最后一个子节点单独拿出来，因为key要少一个*/
		if (!n.leaf) {
			traverse(n.points[i]);
		}
	}

	public Value search(Key key) {
		return root == null ? null : search(root, key);
	}

	/**
	 * 查找k，返回节点
	 *
	 * @author jimo
	 * @date 2018/9/17 8:58
	 */
	public Value search(Node n, Key key) {
		int i = 0;
		/*找到第一个>=k的位置*/
		while (i < n.numOfKey && less(n.entries[i].key, key)) {
			i++;
		}
		if (eq(key, n.entries[i].key)) {
			return (Value) n.entries[i].value;
		}
		if (n.leaf) {
			return null;
		}
		return search(n.points[i], key);
	}

	public void insert(Key key, Value value) {
		if (root == null) {
			root = new Node(k, true);
			root.entries[0] = new Entry(key, value);
			root.numOfKey = 1;
		} else {
			if (root.numOfKey == 2 * k - 1) {
				/**/
				final Node newRoot = new Node(k, false);
				newRoot.points[0] = root;
				splitChild(newRoot, 0, root);
				int i = 0;
				if (less(newRoot.entries[0].key, key)) {
					i++;
				}
				insertNotFull(newRoot.points[i], key, value);
				this.root = newRoot;
			} else {
				insertNotFull(root, key, value);
			}
		}
	}


	/**
	 * 插入没有满的节点
	 *
	 * @author jimo
	 * @date 2018/9/17 9:22
	 */
	public void insertNotFull(Node n, Key key, Value value) {
		int i = n.numOfKey - 1;
		if (n.leaf) {
			/*叶子节点，1.找到key应该插入的位置，2.移出来一个位置*/
			while (i >= 0 && less(key, n.entries[i].key)) {
				n.entries[i + 1] = n.entries[i];
				i--;
			}
			/*插入新的key*/
			n.entries[i + 1] = new Entry(key, value);
			n.numOfKey += 1;
		} else {/*如果不是叶子节点*/
			/*那就得插入到子树得节点里去*/
			/*找到该插入哪个子节点*/
			while (i >= 0 && less(key, n.entries[i].key)) {
				i--;
			}
			/*如果子节点满了*/
			if (n.points[i + 1].numOfKey == 2 * k - 1) {
				/*则需要拆分*/
				splitChild(n, i + 1, n.points[i + 1]);
				/*在拆分后，中间得points[i]指向的子节点会分成2半，下面是判断k应该插入哪一半*/
				if (less(n.entries[i + 1].key, key)) {
					i++;
				}
			}
			insertNotFull(n.points[i + 1], key, value);
		}
	}

	/**
	 * 拆分节点
	 *
	 * @author jimo
	 * @date 2018/9/17 9:41
	 */
	public void splitChild(Node n, int i, Node leftNode) {
		final Node rightNode = new Node(k, leftNode.leaf);

		rightNode.numOfKey = k - 1;

		/*将后半部分的k-1个key copy到新节点*/
		for (int j = 0; j < k - 1; j++) {
			rightNode.entries[j] = leftNode.entries[j + k];
		}
		/*复制后半部分k个节点指针（比key多1）到新节点*/
		if (!leftNode.leaf) {
			for (int j = 0; j < k; j++) {
				rightNode.points[j] = leftNode.points[j + k];
			}
		}
		/*node节点key数量变化*/
		leftNode.numOfKey = k - 1;

		/*往后移一位，腾出新节点的位置*/
		for (int j = n.numOfKey; j >= i + 1; j--) {
			n.points[j + 1] = n.points[j];
		}
		n.points[i + 1] = rightNode;

		for (int j = n.numOfKey - 1; j >= i; j--) {
			n.entries[j + 1] = n.entries[j];
		}
		n.entries[i] = leftNode.entries[k - 1];

		n.numOfKey += 1;
	}

	private boolean eq(Comparable k1, Comparable k2) {
		if (k1 == null || k2 == null) {
			return false;
		}
		return k1.compareTo(k2) == 0;
	}

	private boolean less(Comparable k1, Comparable k2) {
		if (k1 == null || k2 == null) {
			return false;
		}
		return k1.compareTo(k2) < 0;
	}
}
