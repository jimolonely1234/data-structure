package com.jimo.btree;

/**
 * B树的一个节点, 为了简单，属性不设为private，不用getter/setter了
 *
 * @author jimo
 * @date 2018/9/17 8:53
 */
public class Node {
	Entry[] entries;
	Node[] points;
	int numOfKey;
	boolean leaf;

	public Node(int k, boolean leaf) {
		this.leaf = leaf;
		/*节点最多存m-1个key*/
		this.entries = new Entry[2 * k - 1];
		this.numOfKey = 0;
		/*一个节点最多有m个子节点,每个指向一个子节点*/
		this.points = new Node[2 * k];
	}

}
