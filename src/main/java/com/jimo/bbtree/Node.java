package com.jimo.bbtree;

// helper B-tree node data type
public final class Node {
    // max children per B-tree node = M-1
    // (must be even and greater than 2)
    public static final int M = 4;

    public int m;                             // number of children
    public Entry[] children = new Entry[M];   // the array of children

    // create a node with k children
    public Node(int k) {
        m = k;
    }
}
