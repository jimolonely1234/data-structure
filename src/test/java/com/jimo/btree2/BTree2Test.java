package com.jimo.btree2;

import org.junit.Before;
import org.junit.Test;

public class BTree2Test {

	private BTree<Integer, String> bTree;

	@Before
	public void init() {
		bTree = new BTree<>(4);
	}

	@Test
	public void insert() {
		for (int i = 1; i < 8; i++) {
			bTree.insert(i, "i=" + i);
		}
		System.out.println(bTree.toString());
	}

	@Test
	public void search() {
	}
}
