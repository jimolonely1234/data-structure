package com.jimo.bbtree;

import org.junit.Before;
import org.junit.Test;

public class BTreeTest {

    private BTree<Integer,String> bTree;

    @Before
    public void init() {
        bTree = new BTree<>();
    }

    @Test
    public void test() {
        for (int i = 0; i < 20; i++) {
            bTree.put(i,"i="+i);
        }
        System.out.println(bTree.toString());

    }
}
