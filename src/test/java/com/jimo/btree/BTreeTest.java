package com.jimo.btree;


import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class BTreeTest {

	private BTree<Integer, String> bTree;

	@Before
	public void init() {
		bTree = new BTree<>(3);
	}

	@Test
	public void insert() {
		for (int i = 1; i <= 10; i++) {
			bTree.insert(i, "i=" + i);
		}

		bTree.traverse();

		assertNull(bTree.search(100));
		assertNotNull(bTree.search(7));
	}

	@Test
	public void test() {
		List<Integer> arr = new ArrayList<>();
		arr.add(11);
		arr.add(11);
		arr.add(1, 100);
		System.out.println(arr.size());

//        System.arraycopy();
	}
}
