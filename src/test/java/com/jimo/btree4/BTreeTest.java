package com.jimo.btree4;


import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class BTreeTest {

	private BTree<Integer, Integer> bTree;

	@Test
	public void insert() {
		bTree = new BTree<>(2);
		for (int i = 0; i < 10; i++) {
			bTree.insert(i, i);
		}
		bTree.output();
	}

	@Test
	public void insert2() {
		bTree = new BTree<>(2);
		bTree.insert(1, 1);
		bTree.insert(4, 4);
		bTree.insert(7, 7);
		bTree.insert(2, 2);
		bTree.output();
	}

	@Test
	public void search() {
		bTree = new BTree<>(2);
		for (int i = 0; i < 10; i++) {
			bTree.insert(i, i);
		}
		bTree.output();

		assertEquals(new Integer(4), bTree.search(4));
		assertNull(bTree.search(100));
	}

	/**
	 * 删除叶子节点中的key，以及删除叶子后为空的情况？
	 *
	 * @author jimo
	 * @date 2018/10/9 13:49
	 */
	@Test
	public void delete1() {
		bTree = new BTree<>(2);
		bTree.insert(2, 2);
		bTree.insert(5, 5);
		bTree.insert(8, 8);
		bTree.insert(0, 0);
		bTree.insert(10, 10);
		bTree.output();
		bTree.delete(8);
		bTree.output();
		bTree.delete(10);
		bTree.output();
	}

	/**
	 * 从根节点出发，先是情况3b，然后是2a
	 *
	 * @author jimo
	 * @date 2018/10/9 12:40
	 */
	@Test
	public void delete2ab() {
		bTree = new BTree<>(2);
		bTree.insert(1, 1);
		bTree.insert(2, 2);
		bTree.insert(3, 3);
		bTree.insert(4, 4);
		bTree.insert(7, 7);
		bTree.insert(8, 8);
		bTree.insert(5, 5);
		bTree.insert(6, 6);
		bTree.insert(9, 9);
		bTree.output();
		bTree.delete(7);
		bTree.output();
	}

	/**
	 * 删除的key在当前节点，且左右孩子节点都只有t-1个key
	 *
	 * @author jimo
	 * @date 2018/10/9 13:52
	 */
	@Test
	public void delete2c() {
		bTree = new BTree<>(2);
		bTree.insert(5, 5);
		bTree.insert(2, 2);
		bTree.insert(7, 7);
		bTree.insert(6, 6);
		bTree.insert(8, 8);
		bTree.insert(9, 9);
		bTree.output();
		bTree.delete(5);
		bTree.output();
	}

	/**
	 * 删除的key不在当前节点，在子节点，且至少有一个左右子节点的key>=t个
	 *
	 * @author jimo
	 * @date 2018/10/9 14:13
	 */
	@Test
	public void delete3a() {
		bTree = new BTree<>(2);
		for (int i = 1; i <= 4; i++) {
			bTree.insert(i, i);
		}
		bTree.output();
		bTree.delete(1);
		bTree.output();
	}

	/**
	 * 删除的key不在当前节点，但在子节点&&所有相邻兄弟的key数都=t-1
	 *
	 * @author jimo
	 * @date 2018/10/9 14:17
	 */
	@Test
	public void delete3b() {
		bTree = new BTree<>(2);
		for (int i = 1; i <= 4; i++) {
			bTree.insert(i, i);
		}
		bTree.output();
		bTree.delete(1);
		bTree.output();
		bTree.delete(3);
		bTree.output();
	}

	/**
	 * 通过删除不存在的键改变树的结构
	 *
	 * @author jimo
	 * @date 2018/10/9 12:34
	 */
	@Test
	public void deleteToReFormat() {
		bTree = new BTree<>(2);
		for (int i = 0; i < 9; i++) {
			bTree.insert(i, i);
		}
		bTree.output();
		bTree.delete(9);
		bTree.output();
	}
}
